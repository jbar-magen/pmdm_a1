package utad.pmdm_a1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

public class ActivityCentral extends ActionBarActivity {

    TextView txtview_nombre=null;
    TextView txtview_apellidos=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_central);

        txtview_nombre=(TextView)findViewById(R.id.txtview_nombre);
        txtview_apellidos=(TextView)findViewById(R.id.txtview_apellidos);

        // Get the message from the intent
        Intent intent = getIntent();
        String sNombre = intent.getStringExtra("VARIABLE_NOMBRE");
        String sApellidos = intent.getStringExtra("VARIABLE_APELLIDOS");

        txtview_nombre.setText(sNombre);
        txtview_apellidos.setText(sApellidos);
    }

}
